<?php
/**
 * Created by PhpStorm.
 * User: kuangthien
 * Date: 12/18/15
 * Time: 11:43 AM
 */

class SM_FeaturedProducts_Block_FeaturedProducts extends Mage_Core_Block_Template
{
    public function getProductAttribute()
    {
        $att = '';
        if (Mage::getSingleton('cms/page')->getIdentifier() == 'home' &&
            Mage::app()->getFrontController()->getRequest()->getRouteName() == 'cms'
        ) {
            $att = 'global_featured';
        } elseif (Mage::registry('current_category')) {
            $att = 'featured_product';
        }

        return $att;
    }

    /**
     * @return bool|null
     */
    public function getFeaturedProducts()
    {
        $productCollection = Mage::getResourceModel('catalog/product_collection');
        $att = $this->getProductAttribute();
        if (!empty($att)) {
            if($att == 'global_featured'){
                $productCollection = Mage::getModel('catalog/product')->getCollection();
            }else if ($att == 'featured_product') {
                $productCollection = Mage::registry('current_category')->getProductCollection();
            }
        }
        if ($productCollection->getSize()) {
            $productCollection->addAttributeToSelect('*')
                ->addAttributeToFilter('status', 1)
                ->addAttributeToFilter($att, 1);
            return $productCollection;
        }

        return false;
    }
}